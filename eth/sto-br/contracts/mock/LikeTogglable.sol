pragma solidity ^0.4.24;

import "../Togglable.sol";

contract LikeTogglable is Togglable {
  function onlyUpdateIfItIsON(address _profile) public view isON(_profile) returns (bool) {
    return true;
  }
}