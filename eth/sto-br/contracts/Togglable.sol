pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

/**
* @title KYC contract
*/

contract Togglable is Ownable {
    // the list
    mapping(address => bool) private _statuses;

    // emit at the time of change
    event Toggled(address indexed toggle, bool status);

    // update the status
    function _toggle(address _address, bool _status) private onlyOwner {
        require(address(0) != _address, "Invalid address");
        _statuses[_address] = _status;
        emit Toggled(_address, _status);
    }

    function toggleON(address _address) public onlyOwner {
      _toggle(_address, true);
    }

    function toggleOFF(address _address) public onlyOwner {
      _toggle(_address, false);
    }

    modifier isON(address _address) {
      require(address(0) != _address, "Invalid address");
      require(_statuses[_address], "Is toggled off");
      _;
    }
}