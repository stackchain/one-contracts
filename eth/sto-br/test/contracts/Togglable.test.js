const expect = require("expect.js");
const assertRevert = require("../helpers/assertRevert");

const Togglable = artifacts.require("Togglable.sol");
const LikeTogglable = artifacts.require("mock/LikeTogglable.sol");

contract("The Tooglable contract", function ([ deployer, holder1, holder2 ]) {
  let kyc;

  beforeEach(async function () {
    kyc = await Togglable.new();
  });

  describe("update", async function () {
    it("should toggle OFF", async function () {
      const { logs: [ Toggled ] } = await kyc.toggleOFF(holder1);
      expect(Toggled.args).to.eql({ toggle: holder1, status: false });
    });
    it("should toggle ON", async function () {
      const { logs: [ Toggled ] } = await kyc.toggleON(holder1);
      expect(Toggled.args).to.eql({ toggle: holder1, status: true });
    });
    it("should fail toggling ON and is not the OWNER", async function () {
      await assertRevert(kyc.toggleON(holder1, { from: holder1 }));
    });
    it("should fail toggling OFF and is not the OWNER", async function () {
      await assertRevert(kyc.toggleOFF(holder1, { from: holder1 }));
    });
  });

  describe("behavior", async function () {
    it("should return TRUE when ON", async function () {
      let behavior = await LikeTogglable.new();
      const { logs: [ Toggled ] } = await behavior.toggleON(holder1);
      expect(Toggled.args).to.eql({ toggle: holder1, status: true });
      expect(await behavior.onlyUpdateIfItIsON(holder1)).to.be(true);
    });
    it("should return FALSE when OFF", async function () {
      let behavior = await LikeTogglable.new();
      await assertRevert(behavior.onlyUpdateIfItIsON(holder2));
    });
  });
});
